Researching Code Coverage
===========================

Run:

```
./test.sh [optional-argument-to-the-binary]
```

Example:

```
$ ./test.sh sdgfsdf
foo
yes
main
foo
yes
main
    1|       |#include <stdio.h>
    2|       |
    3|      2|void foo() { printf("foo\n"); if (false) {printf("hello\n");} }
    4|       |
    5|      1|int main(int argc, char **argv) {
    6|      1|  if (argc == 2) {
    7|      1|    foo();
    8|      1|    printf("yes\n");
    9|      0|  } else {
   10|      0|    printf("yo\n");
   11|      0|  }
   12|      1|  printf("main\n");
   13|      1|  if (argc == 2) {
   14|      1|    foo();
   15|      1|    printf("yes\n");
   16|      0|  } else {
   17|      0|    printf("yo\n");
   18|      0|  }
   19|      1|  printf("main\n");
   20|      1|}

```

Please note that, in the terminal, the not-covered parts will be highlighted in
red. Surprisingly, the inline if condition is also highlighted correctly.


Steps
------------

- [x] Test the coverage tool with a toy C code.
- [ ] Compile a libfuzzer target with the coverage flag.
- [ ] Run the libfuzzer target and see the coverage result.
- [ ] Understand how the coverage data is structured.

