#!/bin/bash
#

mkdir -p bin

rm -f bin/cov_bin

clang -o bin/cov_bin -fprofile-instr-generate -fcoverage-mapping cov.cc && \
  ./bin/cov_bin $@ && \
  ~/projects/llvm-build/bin/llvm-profdata merge -o cov_bin.profdata default.profraw && \
  ~/projects/llvm-build/bin/llvm-cov show ./bin/cov_bin -instr-profile=cov_bin.profdata cov.cc


