#include <stdio.h>

void foo() { printf("foo\n"); if (false) {printf("hello\n");} }

int main(int argc, char **argv) {
  if (argc == 2) {
    foo();
    printf("yes\n");
  } else {
    printf("yo\n");
  }
  printf("main\n");
  if (argc == 2) {
    foo();
    printf("yes\n");
  } else {
    printf("yo\n");
  }
  printf("main\n");
}
